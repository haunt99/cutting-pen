$(function () {
  var _penCutout = new penCutout();
  _penCutout.init({
    drawPanel: "drawPanel",
    imgSrc: "images/target.jpg",
    penColor: "#ff40ef",
    width: 400,
    height: 400,
  });

  $("#btnPoints").click(function () {
    alert(JSON.stringify(_penCutout.can.pointList));
  });
  $("#btnCut").click(function () {
    _penCutout.createCutImg(function (imgSrcData, w, h) {
      $("#imgCutShow")
        .attr("src", imgSrcData)
        .css({ display: "block", width: w, height: h });
    });
  });
  $("#btnDown").click(function () {
    _penCutout.downLoad();
  });
  $("#redo").click(function () {
    _penCutout.ReDo();
  });
});
