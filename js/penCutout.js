var penCutout = function () {
  this.defaults = {
    drawPanel: "drawPanel",
    canvasId: "canvas",
    imgId: "imgCut",
    width: 400,
    height: 400,
    imgSrc: "images/target.jpg",
    imgBackSrc: "images/tranback.png",
    penColor: "#0087C4",
    defaultPointList: new Array(),
    showTip: function (msg) {
      alert(msg);
    },
  };
  this.init = function (options) {
    if (this.notEmptyObj(options)) {
      this.defaults = $.extend(this.defaults, options);
    }
    this.initElement();
    this.iniData();
    this.eventBind();
  };
  this.initElement = function () {
    $("#drawPanel").append(
      '<img id="' +
        this.defaults.imgId +
        '"/><canvas id="' +
        this.defaults.canvasId +
        '"/>'
    );
  };
  this.eventBind = function () {
    var a = this;
    $("#" + a.can.id).mousemove(function (e) {
      var p = a.can.pointList;
      if (a.can.paint) {
        if (p.length > 0) {
          a.equalStartPoint(p[p.length - 1].pointx, p[p.length - 1].pointy);
        }
        a.roundIn(e.offsetX, e.offsetY);
      }

      a.AddNewNode(e.offsetX, e.offsetY);

      a.draAllMove(e.offsetX, e.offsetY);
    });
    $("#" + a.can.id).mousedown(function (e) {
      var p = a.can.pointList;
      a.can.paint = true;

      if (a.can.tempPointList.length > 0) {
        a.can.pointList.splice(
          a.can.tempPointList[1].pointx,
          0,
          new a.point(
            a.can.tempPointList[0].pointx,
            a.can.tempPointList[0].pointy
          )
        );

        a.can.tempPointList.length = 0;
      }
    });
    $("#" + a.can.id).mouseup(function (e) {
      var p = a.can.pointList;

      a.can.paint = false;

      if (a.can.juPull) {
        a.can.juPull = false;
        a.can.curPointIndex = 0;

        a.equalStartPoint(p[p.length - 1].pointx, p[p.length - 1].pointy);
      } else {
        if (!a.can.IsClose) {
          p.push(new a.point(e.offsetX, e.offsetY));

          a.equalStartPoint(p[p.length - 1].pointx, p[p.length - 1].pointy);

          if (p.length > 1) {
            a.drawLine(
              p[p.length - 2].pointx,
              p[p.length - 2].pointy,
              p[p.length - 1].pointx,
              p[p.length - 1].pointy
            );
            a.drawArc(p[p.length - 1].pointx, p[p.length - 1].pointy);
          } else {
            a.drawArc(p[p.length - 1].pointx, p[p.length - 1].pointy);
          }
        } else {
        }
      }

      if (a.can.IsClose) {
        a.fillBackColor();
        a.drawAllLine();
      }
    });
    $("#" + a.can.id).mouseleave(function (e) {
      a.can.paint = false;
    });
  };
  this.iniData = function (options) {
    if (this.notEmptyObj(options)) {
      this.defaults = $.extend(this.defaults, options);
    }
    this.can.id = this.defaults.canvasId;
    this.can.roundr = 7;
    this.can.roundrr = 3;
    this.can.imgBack.src = this.defaults.imgBackSrc;
    this.can.penColor = this.defaults.penColor;
    this.can.canvas = document.getElementById(this.can.id).getContext("2d");
    this.can.w = this.defaults.width;
    this.can.h = this.defaults.height;
    $(
      "#" +
        this.defaults.drawPanel +
        ",#" +
        this.can.id +
        ",#" +
        this.defaults.imgId
    )
      .attr({
        width: this.defaults.width,
        height: this.defaults.height,
      })
      .css({
        position: "absolute",
        width: this.defaults.width,
        height: this.defaults.height,
      });
    $("#" + this.defaults.drawPanel).css("z-index", 0);
    $("#" + this.defaults.imgId).css("z-index", 1);
    $("#" + this.can.id).css("z-index", 2);
    this.can.curPointIndex = 0;

    this.img.w = this.can.w;
    this.img.h = this.can.h;
    this.img.image.src = this.defaults.imgSrc;
    $("#" + this.defaults.imgId).attr({ src: this.img.image.src });

    this.ReDo();
    if (
      this.notEmptyObj(this.defaults.defaultPointList) &&
      this.defaults.defaultPointList.length > 0
    ) {
      this.setOriPoints(this.defaults.defaultPointList);
    }
  };
  this.point = function (x, y) {
    this.pointx = x;
    this.pointy = y;
  };

  this.setOriPoints = function (pointObj) {
    this.clearCan();
    if (pointObj != null && pointObj.length > 0) {
      this.can.pointList = pointObj.concat();
      if (
        pointObj.length > 1 &&
        pointObj[pointObj.length - 1].pointx == pointObj[0].pointx
      ) {
        this.can.IsClose = true;
        this.fillBackColor();
      } else {
        this.drawAllLine();
      }
    }
  };

  this.img = {
    image: new Image(),
    id: "",
    w: 0,
    h: 0,
  };

  this.can = {
    canvas: new Object(),
    id: "",
    w: 0,
    h: 0,

    pointList: new Array(),

    tempPointList: new Array(),

    roundr: 7,

    roundrr: 7,

    curPointIndex: 0,

    paint: false,

    juPull: false,

    IsClose: false,
    imgBack: new Image(),
    penColor: "#0087C4",
  };

  this.ReDo = function () {
    this.clearCan();

    this.can.pointList.length = 0;

    this.can.IsClose = false;
  };

  this.SaveCut = function () {
    return this.can.pointList();
  };

  this.drawAllLine = function () {
    for (var i = 0; i < this.can.pointList.length - 1; i++) {
      var p = this.can.pointList;
      this.drawLine(p[i].pointx, p[i].pointy, p[i + 1].pointx, p[i + 1].pointy);

      this.drawArc(p[i].pointx, p[i].pointy);
      if (i == this.can.pointList.length - 2) {
        this.drawArc(p[i + 1].pointx, p[i + 1].pointy);
      }
    }
  };

  this.draAllMove = function (x, y) {
    if (!this.can.IsClose) {
      if (this.can.pointList.length >= 1) {
        this.clearCan();
        var p = this.can.pointList;
        for (var i = 0; i < this.can.pointList.length - 1; i++) {
          this.drawLine(
            p[i].pointx,
            p[i].pointy,
            p[i + 1].pointx,
            p[i + 1].pointy
          );

          this.drawArc(p[i].pointx, p[i].pointy);
          if (i == this.can.pointList.length - 2) {
            this.drawArc(p[i + 1].pointx, p[i + 1].pointy);
          }
        }
        if (p.length == 1) {
          this.drawArc(p[0].pointx, p[0].pointy);
        }
        this.drawArcSmall(x, y);
        this.drawLine(
          p[this.can.pointList.length - 1].pointx,
          p[this.can.pointList.length - 1].pointy,
          x,
          y
        );
      }
    }
  };

  this.drawLine = function (startX, startY, endX, endY) {
    this.can.canvas.strokeStyle = this.can.penColor;
    this.can.canvas.lineWidth = 1;
    this.can.canvas.moveTo(startX, startY);
    this.can.canvas.lineTo(endX, endY);
    this.can.canvas.stroke();
  };

  this.drawArc = function (x, y) {
    this.can.canvas.fillStyle = this.can.penColor;
    this.can.canvas.beginPath();
    this.can.canvas.arc(x, y, this.can.roundrr, 360, Math.PI * 2, true);
    this.can.canvas.closePath();
    this.can.canvas.fill();
  };

  this.drawArcSmall = function (x, y) {
    this.can.canvas.fillStyle = this.can.penColor;
    this.can.canvas.beginPath();
    this.can.canvas.arc(x, y, 0.1, 360, Math.PI * 2, true);
    this.can.canvas.closePath();
    this.can.canvas.fill();
  };

  this.drawArcBig = function (x, y) {
    this.can.canvas.fillStyle = this.can.penColor;
    this.can.canvas.beginPath();
    this.can.canvas.arc(x, y, this.can.roundr + 2, 360, Math.PI * 2, true);
    this.can.canvas.closePath();
    this.can.canvas.fill();
  };

  this.showImg = function () {
    this.img.image.onload = function () {
      this.can.canvas.drawImage(this.img.image, 0, 0, this.img.w, this.img.h);
    };
  };

  this.fillBackColor = function () {
    for (var i = 0; i < this.img.w; i += 96) {
      for (var j = 0; j <= this.img.h; j += 96) {
        this.can.canvas.drawImage(this.can.imgBack, i, j, 96, 96);
      }
    }
    this.can.canvas.globalCompositeOperation = "destination-out";
    this.can.canvas.beginPath();
    for (var i = 0; i < this.can.pointList.length; i++) {
      this.can.canvas.lineTo(
        this.can.pointList[i].pointx,
        this.can.pointList[i].pointy
      );
    }
    this.can.canvas.closePath();
    this.can.canvas.fill();
    this.can.canvas.globalCompositeOperation = "destination-over";
    this.drawAllLine();
  };

  this.clearLastPoint = function () {
    this.can.pointList.pop();

    this.clearCan();
    this.drawAllLine();
  };

  this.equalStartPoint = function (x, y) {
    var p = this.can.pointList;
    if (
      p.length > 2 &&
      Math.abs((x - p[0].pointx) * (x - p[0].pointx)) +
        Math.abs((y - p[0].pointy) * (y - p[0].pointy)) <=
        this.can.roundr * this.can.roundr
    ) {
      this.can.IsClose = true;
      p[p.length - 1].pointx = p[0].pointx;
      p[p.length - 1].pointy = p[0].pointy;
    } else {
      this.can.IsClose = false;
    }
  };

  this.clearCan = function () {
    this.can.canvas.clearRect(0, 0, this.can.w, this.can.h);
  };

  this.roundIn = function (x, y) {
    var p = this.can.pointList;
    if (!this.can.juPull) {
      for (var i = 0; i < p.length; i++) {
        if (
          Math.abs((x - p[i].pointx) * (x - p[i].pointx)) +
            Math.abs((y - p[i].pointy) * (y - p[i].pointy)) <=
          this.can.roundr * this.can.roundr
        ) {
          this.can.juPull = true;

          this.can.curPointIndex = i;
          p[i].pointx = x;
          p[i].pointy = y;

          this.clearCan();

          if (this.can.IsClose) {
            this.fillBackColor();
          }
          this.drawAllLine();
          return;
        }
      }
    } else {
      p[this.can.curPointIndex].pointx = x;
      p[this.can.curPointIndex].pointy = y;

      this.clearCan();
      if (this.can.IsClose) {
        this.fillBackColor();
      }
      this.drawAllLine();
    }
  };

  this.AddNewNode = function (newx, newy) {
    var ii = 0;
    if (this.can.IsClose) {
      var p = this.can.pointList;
      for (var i = 0; i < p.length - 1; i++) {
        var result = false;
        if (parseFloat(p[i + 1].pointx) - parseFloat(p[i].pointx) != 0) {
          var k =
            parseFloat(p[i + 1].pointy - p[i].pointy) /
            (p[i + 1].pointx - p[i].pointx);
          var b = p[i].pointy - k * p[i].pointx;
          var userK = parseFloat(k * newx + b);
          if (
            ((userK < newy + 4 && userK > newy - 4) ||
              parseInt(userK) == parseInt(newy)) &&
            (newx - p[i + 1].pointx) * (newx - p[i].pointx) <= 2 &&
            (newy - p[i + 1].pointy) * (newy - p[i].pointy) <= 2
          ) {
            var aa = Math.abs(p[i + 1].pointx - p[i].pointx - 3);
            var ab = Math.abs(p[i + 1].pointx - newx);
            var ac = Math.abs(newx - p[i].pointx);
            var ba = Math.abs(p[i + 1].pointy - p[i].pointy - 3);
            var bb = Math.abs(p[i + 1].pointy - newy);
            var bc = Math.abs(newy - p[i].pointy);
            if (aa <= 2 || aa >= 4) {
              if (ab <= aa && ac <= aa && bb <= ba && bc <= ba) {
                result = true;
              }
            } else {
              if (ab <= aa && ac <= aa) {
                result = true;
              }
            }
          }
        }

        if (
          parseFloat(p[i + 1].pointx) - parseFloat(p[i].pointx) == 0 ||
          Math.abs(parseFloat(p[i + 1].pointx) / parseFloat(p[i].pointx)) >= 15
        ) {
          if (p[i].pointx + 3 >= newx && p[i].pointx - 3 <= newx) {
            var ba = Math.abs(p[i + 1].pointy - p[i].pointy - 3);
            var bb = Math.abs(p[i + 1].pointy - newy);
            var bc = Math.abs(newy - p[i].pointy);
            if (bb <= ba && bc <= ba) {
              result = true;
            }
          }
        }
        if (result) {
          this.can.tempPointList[0] = new this.point(newx, newy);
          this.can.tempPointList[1] = new this.point(i + 1, i + 1);
          i++;

          if (this.can.tempPointList.length > 0) {
            this.clearCan();

            if (this.can.IsClose) {
              this.fillBackColor();
            }
            this.drawAllLine();
            this.drawArcBig(
              this.can.tempPointList[0].pointx,
              this.can.tempPointList[0].pointy
            );
            return;
          }
          return;
        } else {
        }
      }
      if (ii == 0) {
        if (this.can.tempPointList.length > 0) {
          this.can.tempPointList.length = 0;

          this.clearCan();

          if (this.can.IsClose) {
            this.fillBackColor();
          }
          this.drawAllLine();
        }
      }
    } else {
      if (this.can.tempPointList.length > 0) {
        this.can.tempPointList.length = 0;
      }
    }
  };
  this.notEmptyObj = function (obj) {
    if (obj != null && obj != undefined && obj != "") {
      return true;
    }
    return false;
  };
  this.createCutImg = function (fun) {
    var tempPointArray;
    var tempPointList;
    if (this.notEmptyObj(this.can.pointList) && this.can.pointList.length > 1) {
      tempPointList = JSON.parse(JSON.stringify(this.can.pointList));
      tempPointArray = this.movePointArray(tempPointList);
    } else {
      this.defaults.showTip("Please do the cutout operation first");
      return;
    }
    var proxy = this;
    var img = new Image();
    img.crossOrigin = "Anonymous";
    img.src = this.defaults.imgSrc;
    img.onload = function () {
      var canvas = document.createElement("canvas");
      canvas.width = tempPointArray[1].pointx - tempPointArray[0].pointx;
      canvas.height = tempPointArray[1].pointy - tempPointArray[0].pointy;
      var ctx = canvas.getContext("2d");
      ctx.beginPath();
      ctx.moveTo(0, 0);
      for (var i = 0; i < tempPointList.length; i++) {
        ctx.lineTo(tempPointList[i].pointx, tempPointList[i].pointy);
      }
      ctx.lineTo(tempPointList[0].pointx, tempPointList[0].pointy);
      ctx.clip();
      ctx.drawImage(
        img,
        tempPointArray[0].pointx * -1,
        tempPointArray[0].pointy * -1,
        proxy.img.w,
        proxy.img.h
      );
      fun(canvas.toDataURL("image/png"), canvas.width, canvas.height);
    };
  };
  this.downLoad = function () {
    var tempPointArray;
    var tempPointList;
    if (this.notEmptyObj(this.can.pointList) && this.can.pointList.length > 1) {
      tempPointList = JSON.parse(JSON.stringify(this.can.pointList));
      tempPointArray = this.movePointArray(tempPointList);
    } else {
      this.defaults.showTip("Please do the cutout operation first");
      return;
    }
    var proxy = this;
    var img = new Image();
    img.crossOrigin = "Anonymous";
    img.src = this.defaults.imgSrc;
    img.onload = function () {
      var canvas = document.createElement("canvas");
      canvas.width = tempPointArray[1].pointx - tempPointArray[0].pointx;
      canvas.height = tempPointArray[1].pointy - tempPointArray[0].pointy;
      var ctx = canvas.getContext("2d");
      ctx.beginPath();
      ctx.moveTo(0, 0);
      for (var i = 0; i < tempPointList.length; i++) {
        ctx.lineTo(tempPointList[i].pointx, tempPointList[i].pointy);
      }
      ctx.lineTo(tempPointList[0].pointx, tempPointList[0].pointy);
      ctx.clip();
      ctx.drawImage(
        img,
        tempPointArray[0].pointx * -1,
        tempPointArray[0].pointy * -1,
        proxy.img.w,
        proxy.img.h
      );
      var fileName = "target.png";
      if (window.navigator.msSaveOrOpenBlob) {
        var imgData = canvas.msToBlob();
        var blobObj = new Blob([imgData]);
        window.navigator.msSaveOrOpenBlob(blobObj, fileName);
      } else {
        var imgData = canvas.toDataURL("image/png");
        var a = document.createElement("a");
        var event = new MouseEvent("click");
        a.download = fileName;
        a.href = imgData;
        a.dispatchEvent(event);
      }
    };
  };
  this.movePointArray = function (pointArray) {
    var smallX = pointArray[0].pointx;
    var smallY = pointArray[0].pointy;
    var bigX = smallX;
    var bigY = smallY;
    var tempArray = new Array();
    for (var i = 1; i < pointArray.length; i++) {
      if (pointArray[i].pointx < smallX) {
        smallX = pointArray[i].pointx;
      }
      if (pointArray[i].pointx > bigX) {
        bigX = pointArray[i].pointx;
      }
      if (pointArray[i].pointy < smallY) {
        smallY = pointArray[i].pointy;
      }
      if (pointArray[i].pointy > bigY) {
        bigY = pointArray[i].pointy;
      }
    }
    for (var i = 0; i < pointArray.length; i++) {
      pointArray[i].pointx -= smallX;
      pointArray[i].pointy -= smallY;
    }
    tempArray[0] = new this.point(smallX, smallY);
    tempArray[1] = new this.point(bigX, bigY);
    return tempArray;
  };
};
